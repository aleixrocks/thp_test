This application tests under which circumstances Linux Transparent Huge Pages (THP) are allocated.

To do so, several tests are run wich essentially do the following:

 - allocate a range of virtual addresses of a specified size and alignment
 - touch (write) pages within that range to trigger a page fault
 - test how many THP have been allocated before and after this
   operation to figure out whether normal pages (typicalli 4kib) or THP have been used.

Warning! this assumes THP size is 2 MiB. I have not imlemeted
auto-detection. Modify `thp_page size` global for this. 

Have fun! :)
