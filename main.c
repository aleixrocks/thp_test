#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <stdint.h>


const uint64_t four_kib_page = 4*1024;
const uint64_t thp_page_size = 2*1024*1024; // assuming 2 MiB THP
const uint64_t thp_page_mask = (thp_page_size - 1);
const uint64_t thp_align     = thp_page_size;
const uint64_t four_kib_mask = (four_kib_page - 1);


void *alloc_align(size_t size, size_t align, void **raddr, size_t *rsize)
{
	void *addr;

	if (align != 0) {
		size_t asize = size + align;
		uintptr_t mask = (align - 1);
		uintptr_t imsk = ~mask;

		// to get an aligned region, we incrase the region size ensuring
		// that an aligned region with the requested user size will lie
		// within
		addr = mmap(NULL, asize, PROT_READ | PROT_WRITE,
				  MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		if (addr == MAP_FAILED) {
			perror("align mmap error addr");
			exit(EXIT_FAILURE);
		}
		*raddr = addr;
		*rsize = asize;

		return (void *) (((uintptr_t)addr + mask) & imsk);
	} else {
		// if no alignment needed, do a normal malloc
		addr = mmap(NULL, size, PROT_READ | PROT_WRITE,
				  MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		if (addr == MAP_FAILED) {
			perror("mmap error");
			exit(EXIT_FAILURE);
		}
		*raddr = addr;
		*rsize = size;

		return addr;
	}
}

void dealloc(void * addr, size_t size)
{
	if (munmap(addr, size)) {
		perror("munmap");
		exit(EXIT_FAILURE);
	}
}

void touch(void *addr)
{
	*(char *)addr = 1;
}


// parse memory reange with format %p-%p. return 1 if succeed, 0 otherwise
char parse_range(char *l, size_t ls, void **start_addr, void **end_addr)
{
	const char *delim = " ";
	char *start = l;
	char *middle = NULL;
	char *end = NULL;
	int i;
	char ret = 0;

	for (i = 0; (i < ls) && l[i]; i++) {
		if (l[i] == '-') {
			middle = &l[i];
		} else if ((l[i] == ' ') && (middle != NULL)) {
			end = &l[i];
			break;
		} else if (
		   !((l[i] >= 'a') && (l[i] <= 'f') ||
		     (l[i] >= '0') && (l[i] <= '9'))
		) {
			return 0;
		}
	}

	if (middle != NULL && end != NULL) {
		char swap = *middle;
		*middle = '\0';
		*start_addr = (void *) strtol(start, NULL, 16);
		*middle = swap;

		swap = *end;
		*end = '\0';
		*end_addr = (void *) strtol(middle + 1, NULL, 16);
		*end = swap;

		ret = 1;
	}

	return ret;

}

// parse /proc/self/smaps, search for memory mapping containing "addr", and
// return the amount of allocated bytes for THP. If "addr" is not at the
// beginning of the mapping, a warning is issued
size_t get_anon_vma(void *addr)
{
	size_t rls = 0;
	char *line = NULL;
	size_t ls = 0;
	size_t thpsize;
	const char *file="/proc/self/smaps";
	const char *field = "AnonHugePages:";
	const size_t fsize = sizeof(field);
	void *addr_start, *addr_end;
	char exact;

	const int sstrrange = 512;
	char strrange[sstrrange];
	size_t len = snprintf(strrange, sstrrange, "%lx", (uintptr_t) addr);

	FILE *f = fopen(file, "r");
	if (f == NULL) {
		perror("Error reading procsfs file");
		exit(EXIT_FAILURE);
	}

	char found = 0;
	while (getline(&line, &ls, f) != -1) {
		//printf("%s", line);
		if (found == 0) {
			// first serach for the mapping corresponding to the
			// provided address
			if (parse_range(line, ls, &addr_start, &addr_end)) {
				if ((addr >= addr_start) && (addr < addr_end)) {
					found = 1;
					exact = (addr == addr_start);
					if (!exact) {
						fprintf(stderr, "WARNING: mapping was merged into another mapping!\n");
						fprintf(stderr, "         addr: %p range: %p - %p\n", addr, addr_start, addr_end);
					}
					continue;
				}
			}
		} else if (found == 1) {
			// Once the mapping is found, search the requested field
			// for that mapping
			if (!strncmp(field, line, fsize)) {
				char *t;
				const char *delim = " ";
				// skip field name
				t = strtok(line, delim);
				// read value
				t = strtok(NULL, delim);
				thpsize = atoi(t);
				// read unit
				t = strtok(NULL, delim);
				if (strncmp("kB", t, 2)) {
					fprintf(stderr, "unexpected unit: %s\n", t);
					exit(EXIT_FAILURE);
				}
				thpsize *= 1024;
				found = 2;
				break;
			}
		} else {
			fprintf(stderr, "state machine error\n");
			exit(EXIT_FAILURE);
		}
	}

	free(line);
	fclose(f);

	if (found != 2) {
		fprintf(stderr, "failed parsing smaps file with addr: %p %s\n",
			addr, found == 0? "region not found" : "field not found" );
		exit(EXIT_FAILURE);
	}

	return thpsize;
}

// This functions work, but they do not reaturn the real THP size. I don't know
// why.
//size_t get_procfs_field(const char *file, const char *field)
//{
//	size_t rls = 0;
//	char *line = NULL;
//	size_t ls = 0;
//	size_t thpsize;
//	const int fsize = sizeof(field);
//
//	FILE *f = fopen(file, "r");
//	if (f == NULL) {
//		perror("Error reading procsfs file");
//		exit(EXIT_FAILURE);
//	}
//
//	while (getline(&line, &ls, f) != -1) {
//		if (!strncmp(field, line, fsize)) {
//			char *t;
//			const char *delim = " ";
//
//			// skip field name
//			t = strtok(line, delim);
//			// read value
//			t = strtok(NULL, delim);
//			thpsize = atoi(t);
//		}
//	}
//
//	free(line);
//	fclose(f);
//
//	return thpsize;
//}

//size_t get_used_thp_size()
//{
//	const char *field = "AnonHugePages:";
//	return get_procfs_field("/proc/meminfo", field) * 1024;
//}
//
//size_t get_nr_used_thp()
//{
//	const char *field = "nr_anon_transparent_hugepages";
//	return get_procfs_field("/proc/vmstat", field);
//}

// allocate aligned memory and write the first byte of each 4kib page in range.
void test_contiguous_allocation()
{
	size_t base_size = (thp_page_size * 10);
	int n4kib = base_size / four_kib_page;
	size_t thpb, thpa, thpdiff;
	void *base, *rb;
	size_t rbs;
	int algc = 0;
	uintptr_t addr;
	int i;

	base = alloc_align(base_size, thp_align, &rb, &rbs);
	thpb = get_anon_vma(rb);
	addr = (uintptr_t)base;
	for (i = 0; i < n4kib; i++) {
		char thp_aligned = ((addr & (thp_page_mask)) == 0);

		//printf("addr: %p\talign: %s\n",  addr, thp_aligned? "true" : "false");
		touch((void *)addr);

		algc += thp_aligned;
		addr += four_kib_page;
	}
	thpa = get_anon_vma(rb);
	thpdiff = thpa - thpb;

	printf("Touched %zu 4KiB pages, of which %d where 2MiB aligned\n",
	       n4kib, algc);
	printf("%zu THP pages (%zu KiB) where allocated in the process\n",
	       thpdiff/(thp_page_size), thpdiff/1024);

	if ((thpdiff) & thp_page_mask) {
		fprintf(stderr, "Error: calculated THP size is not multiple of THP!\n");
		exit(EXIT_FAILURE);
	}
	dealloc(rb, rbs);
}

// same as test_contiguous_allocation but touching non-page aligned addresses
void test_contiguous_allocation_nonaligned_touch()
{
	size_t base_size = (thp_page_size * 10);
	int n4kib = base_size / four_kib_page;
	size_t thpb, thpa, thpdiff;
	void *base, *rb;
	size_t rbs;
	int algc = 0;
	uintptr_t addr;
	int i;
	size_t increment;

	base = alloc_align(base_size, thp_align, &rb, &rbs);
	thpb = get_anon_vma(rb);
	addr = (uintptr_t)base + 33;
	increment = four_kib_page + 33;
	for (i = 0; i < n4kib; i++) {
		char four_kib_aligned = ((addr & (four_kib_mask)) == 0);

		//printf("addr: %p\talign: %s\n",  addr, thp_aligned? "true" : "false");
		touch((void *)addr);

		algc += four_kib_aligned;
		addr += increment;
	}
	thpa = get_anon_vma(rb);
	thpdiff = thpa - thpb;

	printf("Touched %zu different 4kib pages, of which %d where 4KiB aligned\n",
	       n4kib, algc);
	printf("%zu THP pages (%zu KiB) where allocated in the process\n",
	       thpdiff/(thp_page_size), thpdiff/1024);

	if ((thpdiff) & thp_page_mask) {
		fprintf(stderr, "Error: calculated THP size is not multiple of THP!\n");
		exit(EXIT_FAILURE);
	}
	dealloc(rb, rbs);
}

// allocate non-aligned memory and write the first char only.
void test_single_touch(size_t size)
{
	size_t base_size = size;
	size_t thpb, thpa, thpdiff, nthp;
	void *base, *rb;
	size_t rbs;

	base = alloc_align(base_size, 0, &rb, &rbs);

	thpb = get_anon_vma(base);
	touch(base);
	thpa = get_anon_vma(base);
	thpdiff = thpa - thpb;
	nthp = thpdiff/thp_page_size;

	dealloc(rb, rbs);

	char aligned = !((uintptr_t)base & thp_page_mask);
	if (aligned || (nthp > 0)) {
		printf("allocating %zu KiB (%.2f MiB) of THP-%s memory and touching %p; yields %d THPs\n",
		       base_size/1024, (float)base_size/1024.0/1024.0,
		       (aligned? "aligned" : "non-aligned"),
		       base, nthp);
	}

	if (!aligned && (nthp > 0)) {
		fprintf(stderr, "WARNING: non-aligned mapping yielding an allocated THP. WHAT IS GOING ON?\n");
	}
}



int main()
{
	printf("========== test single touch ============\n");
	int i;
	for (i = 1; i <= (thp_page_size*10)/four_kib_page; i++) {
		// this test does not use alignment, so touching a big region of
		// memory might still not yield a THP being allocated
		test_single_touch(four_kib_page * i);
	}
	printf("========== test continous allocation ============\n");
	test_contiguous_allocation();
	printf("========== test continous allocation with non-4kib-align touches ============\n");
	test_contiguous_allocation_nonaligned_touch();

	return 0;
}
